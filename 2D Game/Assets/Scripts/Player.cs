using System.Collections;
using System.Collections.Generic;
using TMPro;
using UnityEngine;

public class Player : MonoBehaviour
{

    private TMP_Text m_pointsText;
    private int m_points = 0;

    // Start is called before the first frame update
    void Start()
    {
      Component[] textFields = GetComponentsInChildren<TMP_Text>();
      if(textFields.Length > 0)
        {
            m_pointsText = (TMP_Text)textFields[0];
        }
    }

    // Update is called once per frame
    void Update()
    {
        
    }

    void OnCollisionEnter2D(Collision2D collision)
    {
        if(collision.gameObject.tag == "Point")
        {
            m_points++;
            m_pointsText.text = m_points.ToString();
            Destroy(collision.gameObject);
        }
    }
}
